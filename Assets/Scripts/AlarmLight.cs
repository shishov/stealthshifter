﻿using System;
using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Light))]
public class AlarmLight : MonoBehaviour
{
    public float FadeSpeed = 2f;            // How fast the light fades between intensities.
    public float HighIntensity = 2f;        // The maximum intensity of the light whilst the alarm is on.
    public float LowIntensity = 0.5f;       // The minimum intensity of the light whilst the alarm is on.
    public float ChangeMargin = 0.2f;       // The margin within which the target intensity is changed.
    public bool AlarmOn;                    // Whether or not the alarm is on.


    private float targetIntensity;          // The intensity that the light is aiming for currently.
    private Light _light;
    

    void Start()
    {
        _light = gameObject.GetComponent<Light>();

        // When the level starts we want the light to be "off".
        _light.intensity = 0f;

        // When the alarm starts for the first time, the light should aim to have the maximum intensity.
        targetIntensity = HighIntensity;
    }


    void Update()
    {
        // If the light is on...
        if (AlarmOn)
        {
            // ... Lerp the light's intensity towards the current target.
            _light.intensity = Mathf.Lerp(_light.intensity, targetIntensity, FadeSpeed * Time.deltaTime);

            // Check whether the target intensity needs changing and change it if so.
            CheckTargetIntensity();
        }
        else
            // Otherwise fade the light's intensity to zero.
            _light.intensity = Mathf.Lerp(_light.intensity, 0f, FadeSpeed * Time.deltaTime);
    }


    void CheckTargetIntensity()
    {
        // If the difference between the target and current intensities is less than the change margin...
        if (Mathf.Abs(targetIntensity - _light.intensity) < ChangeMargin)
        {
            // ... if the target intensity is high...
            if (Math.Abs(targetIntensity - HighIntensity) < 0.01f)
                // ... then set the target to low.
                targetIntensity = LowIntensity;
            else
                // Otherwise set the targer to high.
                targetIntensity = HighIntensity;
        }
    }
}