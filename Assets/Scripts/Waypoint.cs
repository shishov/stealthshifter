﻿using UnityEngine;
using System.Collections;

public class Waypoint : MonoBehaviour
{
    public float Wait = 5f;

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.yellow;

        Gizmos.DrawRay(transform.position, transform.forward * 2f);
    }
}
