﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{
    public class Stats
    {
        public TimeSpan Time = TimeSpan.Zero;
        public int Alarms;
        public int GuardNeutralized;
        public int ShiftsUsed;
        public int FootSteps;
        public DateTime DateTime = DateTime.Now;
    }

    public static LevelManager Instance { get; private set; }

    public float AlarmTime = 30.0f;
    public AudioClip SuspenseMusic;
    public AudioClip AlertMusic;

    public Vector3 LastKnownPlayerPosition { get; set; }
    public Vector3 PredictedPlayerPosition { get; set; }
    public bool IsAlarm { get; private set; }
    public float CurrentAlarmValue { get; private set; }
    public bool PlayerPositionKnown { get { return AlarmTime - CurrentAlarmValue <= 2f; } }
    

    public Stats CurrentStats { get; private set; }

    private Vector3 _lastPlayerPos = Vector3.zero;
    private Vector3 _ma = Vector3.zero;
    
    private float _alpha = 2f/(10 + 1f); // 5 = number of moving average samples

    private GameObject _playerObject;
    private Player _player;

    void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        _playerObject = GameObject.FindWithTag(Tags.Player);
        _player = _playerObject.GetComponent<Player>();
        
        _lastPlayerPos = _player.transform.position;
        CurrentStats = new Stats();

        MusicManager.Instance.Play(SuspenseMusic);
    }
	
	// Update is called once per frame
	void Update ()
    {
	    if (IsAlarm && _player.IsAlive)
	    {
	        CurrentAlarmValue -= Time.deltaTime;

	        if (CurrentAlarmValue < 0)
	        {
	            if (IsAlarm)
	                OnAlarmEnd();
	            IsAlarm = false;
	        }
	    }

	    PredictedPlayerPosition += _ma / ((PredictedPlayerPosition - _lastPlayerPos).magnitude + 1);  // slighly perfect
	    //PredictedPlayerPosition = _playerObject.transform.position;
    }

    public void SetAlarm(Vector3 newPlayerPosition)
    {
        if(!_player.IsAlive)
            return;

        if (!IsAlarm)
            OnAlarmStart();
        
        IsAlarm = true;
        CurrentAlarmValue = AlarmTime;
        LastKnownPlayerPosition = newPlayerPosition;
        
       
        if (_lastPlayerPos == Vector3.zero)
            _lastPlayerPos = newPlayerPosition;
        var playerSpeed = newPlayerPosition - _lastPlayerPos;

        // exp moving average
        var ma = _alpha * playerSpeed + (1 - _alpha)*_ma;

        PredictedPlayerPosition = _lastPlayerPos;

        _lastPlayerPos = newPlayerPosition;
        _ma = ma;
        
    }

    void OnAlarmStart()
    {
        CurrentStats.Alarms++;
        MusicManager.Instance.Play(AlertMusic, 0.5f);
    }

    void OnAlarmEnd()
    {
        MusicManager.Instance.Play(SuspenseMusic);
    }

    public void OnPlayerDeath()
    {
        IsAlarm = false;
        StartCoroutine(RestartRoutine());
    }

    IEnumerator RestartRoutine()
    {
        yield return new WaitForSeconds(2f);
        RestartLevel();
    }

    public void FinishLevel()
    {
        CurrentStats.Time = DateTime.Now - CurrentStats.DateTime;
        UIManager.Instance.ShowResults();
        Camera.main.GetComponent<CameraTracker>().Blur();
    }

    public void RestartLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void NextLevel()
    {
        SceneManager.LoadScene("levelselect");
    }
}
