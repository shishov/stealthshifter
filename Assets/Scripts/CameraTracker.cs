﻿using UnityEngine;
using UnityStandardAssets.ImageEffects;

[RequireComponent(typeof(Camera))]
[RequireComponent(typeof(AudioSource))]
public class CameraTracker : MonoBehaviour
{
    public GameObject Target;
    public float DampTime = 0.7f;
    public Vector3 Offset = new Vector3(0, 30, -10);

    //private Camera _camera;
    private Vector3 _velocity = Vector3.zero;
    private BlurOptimized _blur;
    
    
    void Start ()
    {
        _blur = GetComponent<BlurOptimized>();
        _blur.enabled = false;
        //_camera = GetComponent<Camera>();
        if (Target != null)
        {
            transform.position = Target.transform.position + Offset * 5f;
            transform.LookAt(Target.transform);
        }
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (Target != null)
        {
            transform.position = Vector3.SmoothDamp(transform.position, Target.transform.position + Offset, ref _velocity, DampTime);
        }
    }

    public void Blur()
    {
        _blur.enabled = true;
    }
}
