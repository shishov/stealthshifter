﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(NoiseEmitter))]
[RequireComponent(typeof(CharacterController))]
[RequireComponent(typeof(AudioSource))]
[RequireComponent(typeof(FootSteps))]
public class Player : MonoBehaviour
{
    public enum PlayerState
    {
        Default,
        ShiftingToStatic,
        ShiftingToDynamic,
        ShiftedInStatic,
        ShiftedInDynamic,
        ShiftingBack,
        Dead
    }
    
    public bool CanMove { get { return State == PlayerState.ShiftedInDynamic || State == PlayerState.Default; } }
    public bool IsAlive { get { return State != PlayerState.Dead; } }

    public PlayerState State
    {
        get { return _state; }
        private set
        {
            if (_state != value)
            {
                var old = _state;
                _state = value;
                StateChanged(old);
            }
        }
    }
    
    public float MoveSpeed = 5f;
    public float RotationSpeed = 8f;
    public float ShiftMoveSpeed = 20f;
    public float ShiftRotationSpeed = 10f;
    public float ShiftTime = 2f;
    public float InteractRange = 5f;

    // NOISE PARAMS
    public float StepNoise = 1f;
    public float CarStepNoise = 1f;
    public float ShiftNoise = 5f;
    public float WhistleNoise = 5f;

    // SOUNDS
    public AudioClip ShiftingSound;
    public AudioClip DeathSound;
    public AudioClip AttackSound;
    public AudioClip CarSound;

    private PlayerState _state;
    private Animator _animator;
    private NoiseEmitter _noise;
    private CharacterController _controller;
    private GameObject _particlesPrefab;
    private GameObject _body;
    private GameObject _replacement;
    private AudioSource _audioSource;
    private GameObject _particles;


    // Use this for initialization
    void Start ()
    {
        _animator = GetComponent<Animator>();
        _controller = GetComponent<CharacterController>();
        _noise = GetComponent<NoiseEmitter>();
        _audioSource = GetComponent<AudioSource>();
        _particlesPrefab = Resources.Load<GameObject>("ShiftParticles");
        State = PlayerState.Default;
        _body = transform.Find("body").gameObject;
    }
	
	// Update is called once per frame
	void Update ()
	{
	    if (IsAlive && CanMove)
	    {
	        var nextDir = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
	        var rotSpeed = RotationSpeed;
	        var moveSpeed = MoveSpeed;

	        if (State == PlayerState.ShiftedInDynamic)
	        {
	            rotSpeed = ShiftRotationSpeed;
	            moveSpeed = ShiftMoveSpeed;
	        }

	        nextDir = Vector3.ClampMagnitude(nextDir, 1.0f);
	        _animator.SetFloat("SpeedMul", nextDir.magnitude);

	        if (nextDir != Vector3.zero)
	            transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(nextDir),
	                Time.deltaTime * rotSpeed);

            _controller.Move(nextDir * Time.deltaTime * moveSpeed);
	        _animator.SetFloat("speed", _controller.velocity.magnitude);

            if (State == PlayerState.ShiftedInDynamic)
            {
                if (nextDir.magnitude > 0.1f)
                {
                    if (!_audioSource.isPlaying)
                        _audioSource.Play();

                    _noise.MakeNoise(Time.deltaTime * CarStepNoise * 5f);
                }
                else
                {
                    _audioSource.Stop();
                }
            }
        }

        if (State == PlayerState.Default)
        {
            if (Input.GetButtonDown("Whistle"))
            {
                _noise.MakeNoise(WhistleNoise); // max(mag) = 1.0f, so     
            }

            if (Input.GetButtonDown("Interact"))
            {
                RaycastHit hit;
                if (Physics.Raycast(transform.position + new Vector3(0, 5, 0), transform.forward, out hit, InteractRange))
                {
                    if (hit.transform.tag == Tags.Guard)
                    {
                        _audioSource.PlayOneShot(AttackSound);
                        _animator.SetTrigger("Attack");
                        hit.transform.gameObject.SendMessage("PlayerInteract");
                    }
                }
            }
        }


        if (Input.GetButtonDown("Shift"))
	    {
            if(State == PlayerState.Default)
                State = PlayerState.ShiftingToStatic;

            if(State == PlayerState.ShiftedInStatic)
                State = PlayerState.ShiftingBack;
        }

        if (Input.GetButtonDown("Shift2"))
        {
            if (State == PlayerState.Default)
                State = PlayerState.ShiftingToDynamic;

            if (State == PlayerState.ShiftedInDynamic)
                State = PlayerState.ShiftingBack;
        }

	    
    }

    private void StateChanged(PlayerState old)
    {
        if (State == PlayerState.Dead)
        { 
            StopAllCoroutines();
            _body.SetActive(true);
            if(_replacement) Destroy(_replacement);
            if(_particles) Destroy(_particles);
            _animator.SetTrigger("Death");
            LevelManager.Instance.OnPlayerDeath();
            _audioSource.PlayOneShot(Resources.Load<AudioClip>("hit"));
        }

        // ENTER
        if (State == PlayerState.ShiftingToStatic)
            StartCoroutine(ShiftingCoroutine(PlayerState.ShiftedInStatic));

        // EXIT
        if (old == PlayerState.ShiftingToStatic)
            StopCoroutine(ShiftingCoroutine(PlayerState.ShiftedInStatic));



        // ENTER
        if (State == PlayerState.ShiftingToDynamic)
            StartCoroutine(ShiftingCoroutine(PlayerState.ShiftedInDynamic));

        // EXIT
        if (old == PlayerState.ShiftingToDynamic)
        {
            StopCoroutine(ShiftingCoroutine(PlayerState.ShiftedInDynamic));
        }


        if (State == PlayerState.ShiftingBack)
        {
            StartCoroutine(ShiftingBackCoroutine());
        }

        // EXIT
        if (old == PlayerState.ShiftingBack)
        {
            StopCoroutine(ShiftingBackCoroutine());
        }

        if (old == PlayerState.ShiftedInDynamic)
        {
            _audioSource.clip = null;
        }
    }

    public void Death()
    {
        State = PlayerState.Dead;
    }

    IEnumerator ShiftingCoroutine(PlayerState target)
    {
        LevelManager.Instance.CurrentStats.ShiftsUsed++;
        _audioSource.PlayOneShot(ShiftingSound);
        _noise.MakeNoise(ShiftNoise);
        _body.SetActive(false);
        _particles = Instantiate(_particlesPrefab);
        _particles.transform.position = transform.position;
        
        yield return new WaitForSeconds(ShiftTime);

        if (target == PlayerState.ShiftedInDynamic)
        {
            _replacement = CreateReplacement(false);
            _audioSource.clip = CarSound;
        }

        if (target == PlayerState.ShiftedInStatic)
        {
            _replacement = CreateReplacement(true);
        }
        Destroy(_particles);
        _noise.MakeNoise(ShiftNoise);
        State = target;
    }

    IEnumerator ShiftingBackCoroutine()
    {
        _audioSource.clip = null;
        _audioSource.PlayOneShot(ShiftingSound);
        Debug.Log("SHIFTING BACK!");
        _noise.MakeNoise(ShiftNoise);
        
        var particles = Instantiate(_particlesPrefab);
        particles.transform.position = transform.position;
        Destroy(_replacement);
        yield return new WaitForSeconds(ShiftTime);
        Destroy(particles);
        _noise.MakeNoise(ShiftNoise);
        _body.SetActive(true);
        State = PlayerState.Default;
    }

    public GameObject CreateReplacement(bool staticReplacement = true)
    {
        var assetName = "Shift";
        if (staticReplacement)
        {
            assetName += "S" + Random.Range(1, 4); //3 static assets
        }
        else
        {
            assetName += "D" + Random.Range(1, 1); //2 dynamic assets
        }

        var prefab = Resources.Load<GameObject>(assetName);
        if (prefab == null)
        {
            Debug.LogError("Can't find prefab: " + assetName);
            return null;
        }

        var o = Instantiate(prefab);
        o.transform.position = transform.position + o.transform.position;
        o.transform.rotation = transform.rotation * o.transform.rotation;
        o.transform.SetParent(transform);
        return o;
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.transform.tag == Tags.Finish)
        {
            LevelManager.Instance.FinishLevel();
        }
    }

    // Fired by animation
    void OnFootStep()
    {
        LevelManager.Instance.CurrentStats.FootSteps++;
        if (State == PlayerState.Default)
        {
            _noise.MakeNoise(StepNoise);
        }
    }
}
