﻿using System.Collections.Generic;
using UnityEngine;

public class NoiseEmitter : MonoBehaviour
{
    private readonly List<AudioDetector> _detectors = new List<AudioDetector>();

    public void MakeNoise(float amount)
    {
        foreach (var audioDetector in _detectors)
        {
            audioDetector.ProceedNoiseAmount(amount, transform.position);
        }
    }

    public void AddListener(AudioDetector detector)
    {
        _detectors.Add(detector);
    }


    public void RemoveListener(AudioDetector detector)
    {
        _detectors.Remove(detector);
    }
}
