﻿using UnityEngine;

[RequireComponent(typeof(SphereCollider))]
public class AudioDetector : MonoBehaviour
{
    public float MaxNoiseLevel = 2f;

    public float CurrentNoiseLevel { get; private set; }
    public bool IsDetectedNoise { get; private set; }
    public Vector3 NoiseSource { get; private set; }
    
	void Update ()
    {
	    if (CurrentNoiseLevel > 0)
	        CurrentNoiseLevel -= Time.deltaTime;

	    IsDetectedNoise = CurrentNoiseLevel > MaxNoiseLevel;
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == Tags.Player)
        {
            other.GetComponent<NoiseEmitter>().AddListener(this);
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.tag == Tags.Player)
        {
            other.GetComponent<NoiseEmitter>().RemoveListener(this);
        }
    }

    public void ProceedNoiseAmount(float value, Vector3 source)
    {
        //var decay = (source - transform.position).magnitude;

        if (!IsDetectedNoise)
        {
            //CurrentNoiseLevel += value/(decay + 0.1f); // TODO add distance decay
            if(CurrentNoiseLevel < MaxNoiseLevel)
                CurrentNoiseLevel += value;
            NoiseSource = source;
        }
    }
}
