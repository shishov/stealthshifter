﻿using UnityEngine;
using System.Collections;

public class Tags : MonoBehaviour {

    // A list of tag strings.
    public const string Player = "Player";
    public const string Guard = "Guard";
    public const string Finish = "Finish";
}
