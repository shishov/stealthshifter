﻿using System;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public static UIManager Instance;
    private Image _alertPanel;
    private Text _alertText;

    private readonly Color _trColor = new Color(0f,0f,0f,0f);
    private GameObject _resultPanel;
    private Text _resultText;


    void Awake()
    {
        Instance = this;
        _resultPanel = transform.Find("ResultPanel").gameObject;
        _resultPanel.SetActive(false);
        _resultText = _resultPanel.transform.Find("Text").GetComponent<Text>();
    }
    
    void Start ()
	{
	    _alertPanel = transform.Find("AlertPanel").GetComponent<Image>();
	    _alertPanel.color = new Color(1f,0f,0f,0f);
        _alertText = _alertPanel.transform.FindChild("AlertValue").GetComponent<Text>();
        _alertText.text = LevelManager.Instance.AlarmTime.ToString("00.00");

    }
	
	// Update is called once per frame
	void Update () {
	    if (LevelManager.Instance.IsAlarm)
	    {
            var color = Color.LerpUnclamped(_trColor, Color.red, Mathf.PingPong(Time.time, 0.5f) + 0.5f);
            _alertPanel.color = color;
	        _alertText.color = color;
	        _alertText.text = LevelManager.Instance.CurrentAlarmValue.ToString("00.00");
	    }
	    else
	    {
	        _alertText.text = LevelManager.Instance.AlarmTime.ToString("00.00");
	        _alertText.color = Color.white;
            _alertPanel.color = _trColor;
        }
	}

    public void ShowResults()
    {
        _resultPanel.SetActive(true);

        var stats = LevelManager.Instance.CurrentStats;
        var text = new StringBuilder();
        text.AppendLine(String.Format("Time:\t{0:g}", stats.Time));
        text.AppendLine(String.Format("Alarms:\t{0}", stats.Alarms));
        text.AppendLine(String.Format("Guards neutralized:\t{0}", stats.GuardNeutralized));
        text.AppendLine(String.Format("Shifts:\t{0}", stats.ShiftsUsed));
        text.AppendLine(String.Format("Steps:\t{0}", stats.FootSteps));

        _resultText.text = text.ToString();

        //_resultPanel.
    }
}
