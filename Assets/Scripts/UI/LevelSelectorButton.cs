﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelSelectorButton : MonoBehaviour
{
    public string LevelName;

    public void Start()
    {
        GetComponent<Button>().onClick.AddListener(Click);
    }

    public void Click()
    {
        SceneManager.LoadScene(LevelName);
    }
}
