﻿using System.Collections;
using UnityEngine;

[RequireComponent(typeof(NavMeshAgent))]
[RequireComponent(typeof(AudioSource))]
[RequireComponent(typeof(FootSteps))]
public class Guard : MonoBehaviour
{
    public static int GuardNavPriority = 10; 

    public enum GuardState
    {
        Patroling, // Non alarmed patroling
        WaypointWaiting,
        GoingToNoiseSource, // go to noise source being non-alarmed
        Seek,
        Sleep,

        AlarmPatroling,
        AlarmShooting, // Shooting
        AlarmChasing, // Running toward player
        AlarmSeek, // seek for a player while standing still
        AlarmRunningToNoiseSource, // Run towards noise source being alarmed
    }

    public enum GuardType
    {
        Melee,
        Ranged
    }

    public GameObject[] PatrolWaypoints;
    public float SleepTime = 10f;
    public float MoveSpeed = 5f;
    public float ChaseSpeed = 10f;
    public float RotationSpeed = 1f;
    public float SeekTime = 2f;
    public float AimSpread = 2f;
    public GuardType GuartType = GuardType.Melee;
    public GameObject QuestionMark;
    public GameObject AlertMark;
    public GameObject BulletTrack;

    // AUDIO
    public AudioClip ShotSound;
    public AudioClip QuestionSound;
    public AudioClip AlertSound;
    public AudioClip HitGroundSound;
    
    public GuardState State
    {
        get { return _state; }
        set
        {
            if (_state != value)
            {
                var old = _state;
                _state = value;
                StateChanged(old);
            }
        }
    }

    private NavMeshAgent _agent;
    private GuardState _state;
    private int _targetWaypoint;
    private AudioDetector _detector;
    private VisualDetector _visualDetector;
    private Quaternion _targetRotation;
    private AudioSource _audioSource;

    private readonly Vector3 _weaponOffset = new Vector3(0, 4, 1);
    private readonly Vector3 _headOffset = new Vector3(0, 6, 0);

    // COROUTINES
    private Coroutine _shootingCoroutine;
    private Coroutine _seekCoroutine;
    private Coroutine _alarmSeekCoroutine;
    private Coroutine _waypointWaitCoroutine;

    void Start ()
	{
	    _agent = GetComponent<NavMeshAgent>();
        _audioSource = GetComponent<AudioSource>();
        State = GuardState.Patroling;
	    _targetWaypoint = 0;
        _detector = GetComponent<AudioDetector>();
        _visualDetector = GetComponent<VisualDetector>();
        StateChanged(GuardState.Patroling);

        GetComponent<NavMeshAgent>().avoidancePriority = GuardNavPriority;
        GuardNavPriority++;
	}

    bool IsAlarmedState()
    {
        return State == GuardState.AlarmShooting ||
               State == GuardState.AlarmChasing ||
               State == GuardState.AlarmRunningToNoiseSource ||
               State == GuardState.AlarmSeek ||
               State == GuardState.AlarmPatroling;
    }

    bool IsAlarmSeekingState()
    {
        return State == GuardState.AlarmPatroling ||
               State == GuardState.AlarmSeek ||
               State == GuardState.AlarmRunningToNoiseSource;
    }

    bool IsPatrolingState()
    {
        return State == GuardState.Patroling ||
               State == GuardState.WaypointWaiting;
    }

    bool IsSuspectedState()
    {
        return State == GuardState.Seek||
               State == GuardState.GoingToNoiseSource;
    }

	void Update ()
	{
        if(State == GuardState.Sleep)
            return; // DO NOTHING

	    if (LevelManager.Instance.IsAlarm)
	    {
            if (IsPatrolingState() || IsSuspectedState())
                State = GuardState.AlarmChasing;
            
            if (GuartType == GuardType.Melee && _visualDetector.IsSeeingPlayer)
                State = GuardState.AlarmChasing;

            if ((State == GuardState.AlarmChasing || IsAlarmSeekingState()) && GuartType == GuardType.Ranged && _visualDetector.IsSeeingPlayer)
                State = GuardState.AlarmShooting;

            if (IsAlarmSeekingState() && LevelManager.Instance.PlayerPositionKnown)
	            State = GuardState.AlarmChasing;

            if (IsAlarmSeekingState() && _detector.IsDetectedNoise)
                State = GuardState.AlarmRunningToNoiseSource;

            
        }
        else
	    {
            // If there is no alarm and detects sound
            if (IsPatrolingState() && _detector.IsDetectedNoise)
                State = GuardState.GoingToNoiseSource;

            // Back to patrol if there is no alarm and was in alarmed state
            if (IsAlarmedState())
                State = GuardState.Patroling;
        }

        if (State == GuardState.AlarmChasing)
        {
            _targetRotation = Quaternion.LookRotation(LevelManager.Instance.PredictedPlayerPosition - transform.position,
                transform.up);
            _agent.SetDestination(LevelManager.Instance.PredictedPlayerPosition);
        }


        if (!_agent.hasPath)
            transform.rotation = Quaternion.Slerp(transform.rotation, _targetRotation, RotationSpeed * Time.deltaTime);

        if (_agent.hasPath && !_agent.isPathStale)
        {
            if (_agent.remainingDistance <= _agent.stoppingDistance)
            {
                DestinationReached();
                _agent.ResetPath();
            }
        }

        GetComponent<Animator>().SetFloat("Speed", _agent.velocity.magnitude);
    }

    void OnDrawGizmosSelected()
    {
        // DRAW patrol path
        if (PatrolWaypoints.Length > 0)
        {
            var first = transform.position;
            Gizmos.color = new Color(1, 0, 0, 2.5F);
            foreach (var t in PatrolWaypoints)
            {
                if(t == null)
                    continue;
                Gizmos.DrawLine(first, t.transform.position);
                first = t.transform.position;
            }

            Gizmos.DrawLine(first, PatrolWaypoints[0].transform.position);
        }
    }
    
    void StateChanged(GuardState oldState)
    {
        Debug.Log("STATE: " + State);

        // EXITS ////////////////
        if (oldState == GuardState.AlarmChasing || oldState == GuardState.AlarmRunningToNoiseSource)
        {
            _agent.speed = MoveSpeed;
        }
      
        if (oldState == GuardState.AlarmShooting)
        {
            StopCoroutine(_shootingCoroutine);
        }

        if (oldState == GuardState.Seek)
        {
            GetComponent<Animator>().SetBool("Seek", false);
            StopCoroutine(_seekCoroutine);
        }

        if (oldState == GuardState.AlarmSeek)
        {
            GetComponent<Animator>().SetBool("Seek", false);
            StopCoroutine(_alarmSeekCoroutine);
        }

        if (oldState == GuardState.WaypointWaiting)
        {
            StopCoroutine(_waypointWaitCoroutine);
        }

        if (oldState == GuardState.Sleep)
        {
            StopCoroutine(SleepingRoutine());
            _visualDetector.enabled = true;
        }
        /////////////////////////////////////


        if (State == GuardState.Patroling)
        {
            _agent.SetDestination(PatrolWaypoints[_targetWaypoint].transform.position);
        }

        if (State == GuardState.GoingToNoiseSource)
        {
            _audioSource.PlayOneShot(QuestionSound);
            StartCoroutine(ShowMarkCoroutine(QuestionMark));
            _agent.SetDestination(_detector.NoiseSource);
        }

        if (State == GuardState.AlarmChasing)
        {
            _audioSource.PlayOneShot(AlertSound);
            StartCoroutine(ShowMarkCoroutine(AlertMark));
            _agent.speed = ChaseSpeed;
            _agent.SetDestination(LevelManager.Instance.PredictedPlayerPosition);
        }

        if (State == GuardState.AlarmRunningToNoiseSource)
        {
            _audioSource.PlayOneShot(QuestionSound);
            StartCoroutine(ShowMarkCoroutine(AlertMark));
            _agent.speed = ChaseSpeed;
            _agent.SetDestination(_detector.NoiseSource);
        }

        if (State == GuardState.AlarmShooting)
        {
            _shootingCoroutine = StartCoroutine(ShootingRoutine());
        }

        if (State == GuardState.Seek)
        {
            GetComponent<Animator>().SetBool("Seek", true);
            _seekCoroutine = StartCoroutine(SeekCoroutine());
        }

        if (State == GuardState.AlarmSeek)
        {
            GetComponent<Animator>().SetBool("Seek", true);
            _alarmSeekCoroutine = StartCoroutine(AlarmSeekCoroutine());
        }

        if (State == GuardState.WaypointWaiting)
        {
            _waypointWaitCoroutine = StartCoroutine(WaypointWaitCoroutine());
        }

        if (State == GuardState.AlarmPatroling)
        {
            //var delta = LevelManager.Instance.PredictedPlayerPosition - transform.position;
            _agent.SetDestination(LevelManager.Instance.PredictedPlayerPosition);
        }

        if (State == GuardState.Sleep)
        {
            StartCoroutine(SleepingRoutine());
            _visualDetector.enabled = false;
        }
    }

    void DestinationReached()
    {
        if (_state == GuardState.Patroling)
            State = GuardState.WaypointWaiting;

        if (State == GuardState.GoingToNoiseSource)
            State = GuardState.Seek;

        if (State == GuardState.AlarmChasing ||
            State == GuardState.AlarmRunningToNoiseSource)
        {
            if(!LevelManager.Instance.PlayerPositionKnown)
                State = GuardState.AlarmSeek;
        }

        if (State == GuardState.AlarmPatroling)
            State = GuardState.AlarmSeek;
    }

    IEnumerator WaypointWaitCoroutine()
    {
        var waypoint = PatrolWaypoints[_targetWaypoint];
        _targetRotation = waypoint.transform.rotation;
        yield return new WaitForSeconds(waypoint.GetComponent<Waypoint>().Wait);
        
        if (_targetWaypoint >= PatrolWaypoints.Length - 1)
            _targetWaypoint = 0;
        else
            _targetWaypoint++;
        
        State = GuardState.Patroling;
    }

    IEnumerator SeekCoroutine()
    {
        yield return new WaitForSeconds(SeekTime);
        _targetRotation = transform.rotation * Quaternion.Euler(0, 120, 0);
        yield return new WaitForSeconds(SeekTime);
        State = GuardState.Patroling;
    }

    IEnumerator AlarmSeekCoroutine()
    {
        yield return new WaitForSeconds(SeekTime);
        _targetRotation = transform.rotation * Quaternion.Euler(0, 120, 0);
        yield return new WaitForSeconds(SeekTime);
        _targetRotation = transform.rotation * Quaternion.Euler(0, 120, 0);
        yield return new WaitForSeconds(SeekTime);
        _targetRotation = transform.rotation * Quaternion.Euler(0, 120, 0);
        yield return new WaitForSeconds(SeekTime);
        State = GuardState.AlarmPatroling;
    }

    IEnumerator ShowMarkCoroutine(GameObject markPrefab)
    {
        var mark = (GameObject)GameObject.Instantiate(markPrefab, transform.position + new Vector3(0, 10f, 0), Quaternion.identity);
        mark.transform.SetParent(transform.parent, false);
        yield return new WaitForSeconds(1f);
        Destroy(mark);
    }

    IEnumerator ShootingRoutine()
    {
        // STAND STILL
        _agent.ResetPath();
        GetComponent<Animator>().SetTrigger("Fire");

        // AIM
        var target = LevelManager.Instance.PredictedPlayerPosition + _headOffset;

        var source = transform.position + transform.rotation * _weaponOffset;
        var rayDirection = target - source ;
        _targetRotation = Quaternion.LookRotation(rayDirection, transform.up);

        // WAIT FOR ANIMATION END
        yield return new WaitForSeconds(1f); // eq Fire anim

        State = GuardState.AlarmSeek;
    }
    
    // FIRED BY ANIMATION 
    public void GunFire()
    {
        if(State != GuardState.AlarmShooting)
            return;

        var target = LevelManager.Instance.PredictedPlayerPosition + _headOffset;
        var source = transform.position + transform.rotation * _weaponOffset;
        var rayDirection = target - source;
        rayDirection += Random.insideUnitSphere*AimSpread / (rayDirection.magnitude / AimSpread + 1);


        RaycastHit hit;
        var track = Instantiate(BulletTrack);
        var lr = track.GetComponent<LineRenderer>();
        lr.SetPosition(0, source);
        lr.SetPosition(1, source + rayDirection * 100);

        _targetRotation = Quaternion.LookRotation(rayDirection, transform.up);

        if (Physics.Raycast(source, rayDirection, out hit))
        {
            lr.SetPosition(1, hit.point);
            if (hit.transform.gameObject.tag == Tags.Player)
            {
                //hit.transform.GetComponent<Player>().Death();
            }
            
            
            // TOO BIG COLLIDER
            /*
            if (hit.transform.gameObject.tag == Tags.Guard)
            {
                 hit.transform.gameObject.GetComponent<Guard>().Death();
            }*/
        }

        _audioSource.PlayOneShot(ShotSound);
    }

    public void PlayerInteract()
    {
        State = GuardState.Sleep;
        LevelManager.Instance.CurrentStats.GuardNeutralized++;
    }

    public void Death()
    {
        Debug.Log("Fake death");
        State = GuardState.Sleep;
    }

    IEnumerator SleepingRoutine()
    {
        _agent.ResetPath();
        GetComponent<Animator>().SetTrigger("Sleep");
        yield return new WaitForSeconds(1f);
        _audioSource.PlayOneShot(HitGroundSound);
        yield return new WaitForSeconds(SleepTime - 1f);
        GetComponent<Animator>().SetTrigger("Awake");
        State = GuardState.Seek;
    }

    void OnCollisionEnter(Collision collision)
    {
        Debug.Log("COLLISION");
    }
}
