﻿using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class FootSteps : MonoBehaviour
{
    public AudioClip[] FootStepSounds;
    public float VolumeScale = 1f;

    private AudioSource _source;
    private int _footStepIndex = 0;

    void Start ()
    {
        _source = GetComponent<AudioSource>();
    }

    // Fired by animation
    public void OnFootStep()
    {
        _footStepIndex = (_footStepIndex + 1) % FootStepSounds.Length;
        _source.PlayOneShot(FootStepSounds[_footStepIndex], VolumeScale);
    }
}
