﻿using UnityEngine;

[RequireComponent(typeof(SphereCollider))]
public class VisualDetector : MonoBehaviour {

    public float ViewAngle = 120;
    public float RevealDistance = 10f;
    public string ChildPath = "metarig/hips/spine/chest/neck/head";

    public bool IsSeeingPlayer { get; private set; }
    public GameObject TrackedPlayer { get { return _trackedPlayer; } }

    private SphereCollider _collider;
    private GameObject _trackedPlayer;
    private Transform _head;


    // Use this for initialization
    void Start ()
    {
        _collider = GetComponent<SphereCollider>();
        _head = transform.Find(ChildPath);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == Tags.Player)
        {
            _trackedPlayer = other.gameObject;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.tag == Tags.Player)
        {
            _trackedPlayer = null;
        }
    }
    
    void Update ()
    {
        var res = CanSeePlayer();

        /*
        if (res && !IsSeeingPlayer)
            Debug.Log("Player found!");

        if (!res && IsSeeingPlayer)
            Debug.Log("Player lost!");*/

        IsSeeingPlayer = res;

        if (IsSeeingPlayer)
        {
            LevelManager.Instance.SetAlarm(_trackedPlayer.transform.position);
        }
    }
    
    bool CanSeePlayer()
    {
        if (_trackedPlayer == null)
            return false;
        
        var rayDirection = _trackedPlayer.transform.position - _head.position;
        rayDirection.y = 0;
        var forward = _head.forward;
        forward.y = 0;
        forward.Normalize();
        if (Vector3.Angle(forward, rayDirection) < ViewAngle * 0.5f)
        {
            RaycastHit hit;
            if (Physics.Raycast(_head.position, rayDirection, out hit, _collider.radius))
            {
                if (hit.transform.tag == Tags.Player)
                {
                    // If player is hidden
                    if (_trackedPlayer.GetComponent<Player>().State == Player.PlayerState.ShiftedInStatic)
                        return hit.distance < RevealDistance;
                   
                    return true;
                }               
            }
        }

       return false;
    }
    
    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.magenta; 
        var r = GetComponent<SphereCollider>().radius;


        var t = _head;

        if (t == null)
            t = transform.Find(ChildPath);

        if (t == null)
            return;
        //var c = GetComponent<SphereCollider>().center;
        //Gizmos.DrawRay(transform.position, transform.forward * r);

        var forward = t.forward;
        forward.y = 0;
        forward.Normalize();

        Gizmos.DrawRay(t.position, Quaternion.AngleAxis(-ViewAngle * 0.5f, Vector3.up) * forward * r);
        Gizmos.DrawRay(t.position, Quaternion.AngleAxis(ViewAngle * 0.5f, Vector3.up) * forward * r);

        if (_trackedPlayer != null && _collider != null)
        {
            Gizmos.color = Color.blue;
            var rayDirection = _trackedPlayer.transform.position - t.position;
            Gizmos.DrawRay(t.position, rayDirection);
        }
    }
}
