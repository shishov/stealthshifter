﻿using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class MusicManager : MonoBehaviour
{
    public static MusicManager Instance;

    void Awake()
    {
        Instance = this;
    }

	// Use this for initialization
	void Start ()
    {
    }

    public void Play(AudioClip clip, float delay = 0f)
    {
        var audio = GetComponent<AudioSource>();

        if (clip != null)
        {
            audio.clip = clip;
            if (delay > 0)
                audio.PlayDelayed(delay);
            else
                audio.Play();
        }
        else
        {
            audio.Stop();
        }
    }
}
