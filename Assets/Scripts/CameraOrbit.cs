﻿using UnityEngine;
using System.Collections;

public class CameraOrbit : MonoBehaviour
{

    public float Radius = 20f;
    public float Height = 10f;
    public Vector3 Target = Vector3.zero;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update ()
	{
	    var x = Mathf.Sin(Time.time / 10f);
        var z = Mathf.Cos(Time.time / 10f);
        transform.position = new Vector3(Target.x + x * Radius, Target.y + Height, Target.z + z * Radius);

        transform.LookAt(Target);
	}
}
