﻿using UnityEngine;
using System.Collections;

public class TimeToLive : MonoBehaviour
{
    public float LiveTime = 2f;
	
	void Update ()
	{
        LiveTime -= Time.deltaTime;
        if(LiveTime < 0)
            Destroy(gameObject);
    }
}
