﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(LineRenderer))]
public class BulletTrack : MonoBehaviour
{
    private LineRenderer _lr;
    public float FadeTime = 1f;

    private float _current;
	
	void Start ()
	{
	    _lr = GetComponent<LineRenderer>();
	    _current = FadeTime;
	}
	
	// Update is called once per frame
	void Update ()
	{
	    _current -= Time.deltaTime;
	    if (_current < 0)
	    {
	        Destroy(gameObject);
	    }
	    else
	    {
            var v = _current / FadeTime;
            var color = new Color(v, v, v, v);
            _lr.SetColors(color, color);
        }
	}
}
